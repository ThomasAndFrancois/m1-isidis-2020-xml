<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:param name="country" />
<xsl:param name="continent" />

<xsl:variable name="country_filter"
              select="//country[
                        contains(name, $country)
                        and contains(@continent, $continent)]" />


<xsl:template match="/">
  <table class="table">
    <thead class="thead-light">
      <tr>
        <th>#</th>
        <th>Country</th>
        <th>Continent</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="$country_filter">
	  <xsl:sort select="name" order="ascending"/>
          <tr>
            <td><img width="40" height="25" alt="Responsive image" src="{flag/@src}" /> </td>
            <td><a href="/html/country_infos.html?country={name}"><xsl:value-of select="name"/></a></td>
            <td><xsl:value-of select="@continent"  /></td>
          </tr>
        </xsl:for-each>
      </tbody>
    </table>
  </xsl:template>
</xsl:stylesheet>
