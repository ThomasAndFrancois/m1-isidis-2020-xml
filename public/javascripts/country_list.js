window.onload = async function(){

    const ARGS = { country: '', continent: '' };
    const xml = await get('../xml/countries.xml');
    const xsl = await get('../xsl/country_list.xsl');


    async function display() {

      const { country, continent } = ARGS;
    
    // TODO: xsl transformations
      const proc = new XSLTProcessor();
      proc.importStylesheet(xsl);
      proc.setParameter(null, 'country', country);
      proc.setParameter(null, 'continent', continent);
      const result = proc.transformToFragment(xml, document);
 
    // TODO: append in #frag_country_list
   
      while (frag_country_list.firstChild) {
          frag_country_list.removeChild(frag_country_list.firstChild);
       }
       document.querySelector('#frag_country_list').appendChild(result);

    }


    document.querySelector("#search_name").onkeyup = function () {
        ARGS.country = this.value;
        display();
    };

    document.querySelector('#search_continent').onclick = function () {
        ARGS.continent = this.value;
        display();
    };


    display();

};
