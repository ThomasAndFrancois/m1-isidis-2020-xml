function get(url, mode='xml') {
  return new Promise(function (resolve, reject) {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', url);
    xhr.onload = _ => resolve(
      (mode === 'xml') ? xhr.responseXML : xhr.responseText
    );
    xhr.send();
  });
}
