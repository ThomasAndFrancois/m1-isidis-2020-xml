Le nom du 1er pays du document :
/countries/country[1]/name

La capital de l'Azerbaijan :
//country[name="Azerbaijan"]/capital

Le continent du Canada :
string(//country[name = 'Canada']/@continent) 

Le nom du pays qui a pour country code (cc) : fr
//country/name[@cc ='fr']

La population mondiale (somme de la population de tous les pays) ?
sum(//country/population)

Le densité de population (nombre d'habitants au km²) au Japon (Japan) :
//country[name="Japan"]/population div //country[name="Japan"]/area 

Le pourcentage de la population mondiale sur le continent asiatique (Asia) ?
sum(//country[@continent = "Asia"]/population) div sum(//country/population) * 100

Le nombre de pays qui ont une population supérieure à 1 milliard d'habitants (1e9) 
count(//country[population > 1000000000])